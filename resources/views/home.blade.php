@extends('layouts.master')

@section('content')
    <article class="content dashboard-page">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-sm-6 p-md-0">
                    <div class="title">
                        <h1 class="mb-0">Reports</h1>
                    </div>
                </div>
                <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                    <button  class="btn btn-filter   bg-transparent"><img src="{{asset('img/filter.svg')}}" alt="">Filters</button>
                    <button  class="btn btn-add-task  btn-primary"><a href="#"><img src="{{asset('img/add_task.svg')}}" alt="">Add Task</a></button>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <table id="reports-table" class="stripe table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="align-center"> <input type="radio" name="Name" id=""> REPORT TITLE</th>
                            <th>DATE CREATED</th>
                            <th>STATUS</th>
                            <th>CREATED BY</th>
                            <th>DUE</th>
                            <th>WEEK</th>
                            <th class="no-sort"></th>
                        </tr>
                        </thead>
                        <tfoot>
                        </tfoot>

                        <tbody>
                        <tr>
                            <td>Foundation Work</td>
                            <td>2008/11/13</td>
                            <td><span class="completed text-green">Completed</span></td>
                            <td>Kampala</td>
                            <td>2011/04/25</td>
                            <td>25</td>
                            <td class="text-center has-edit-delete">
                                <div class="dropdown">
                                    <i class="fas fa-ellipsis-h" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <a class="dropdown-item" href="#"> Edit </a>
                                        <a class="dropdown-item text-danger" href="#">Remove</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Foundation Work</td>
                            <td>2008/11/13</td>
                            <td><span class="pending text-orange">Pending</span></td>
                            <td>Santiago</td>
                            <td>2011/07/25</td>
                            <td>25</td>
                            <td class="text-center has-edit-delete">
                                <div class="dropdown">
                                    <i class="fas fa-ellipsis-h" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <a class="dropdown-item" href="#"> Edit </a>
                                        <a class="dropdown-item text-danger" href="#">Remove</a>
                                    </div>
                                </div>
                            </td>

                        </tr>

                        <tr>
                            <td>Foundation Work</td>
                            <td>2008/11/13</td>
                            <td><span class="completed text-green">Completed</span></td>
                            <td>Kampala</td>
                            <td>2009/02/27</td>
                            <td>25</td>
                            <td class="text-center has-edit-delete">
                                <div class="dropdown">
                                    <i class="fas fa-ellipsis-h" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <a class="dropdown-item" href="#"> Edit </a>
                                        <a class="dropdown-item text-danger" href="#">Remove</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Foundation Work</td>
                            <td>2008/11/13</td>
                            <td><span class="pending text-orange">Pending</span></td>
                            <td>Santiago</td>
                            <td>2010/07/14</td>
                            <td>25</td>
                            <td class="text-center has-edit-delete">
                                <div class="dropdown">
                                    <i class="fas fa-ellipsis-h" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <a class="dropdown-item" href="#"> Edit </a>
                                        <a class="dropdown-item text-danger" href="#">Remove</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Foundation Work</td>
                            <td>2008/11/13</td>
                            <td><span class="completed text-green">Completed</span></td>
                            <td>Santiago</td>
                            <td>2008/11/13</td>
                            <td>25</td>
                            <td class="text-center has-edit-delete">
                                <div class="dropdown">
                                    <i class="fas fa-ellipsis-h" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <a class="dropdown-item" href="#"> Edit </a>
                                        <a class="dropdown-item text-danger" href="#">Remove</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Foundation Work</td>
                            <td>2008/11/13</td>
                            <td><span class="completed text-green">Completed</span></td>
                            <td>Santiago</td>
                            <td>2011/06/27</td>
                            <td>25</td>
                            <td class="text-center has-edit-delete">
                                <div class="dropdown">
                                    <i class="fas fa-ellipsis-h" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <a class="dropdown-item" href="#"> Edit </a>
                                        <a class="dropdown-item text-danger" href="#">Remove</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Foundation Work</td>
                            <td>2008/11/13</td>
                            <td><span class="pending text-orange">Pending</span></td>
                            <td>Santiago</td>
                            <td>2011/01/25</td>
                            <td>25</td>
                            <td class="text-center has-edit-delete">
                                <div class="dropdown">
                                    <i class="fas fa-ellipsis-h" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <a class="dropdown-item" href="#"> Edit </a>
                                        <a class="dropdown-item text-danger" href="#">Remove</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Foundation Work</td>
                            <td>2008/11/13</td>
                            <td><span class="completed text-green">Completed</span></td>
                            <td>Kampala</td>
                            <td>2011/06/27</td>
                            <td>25</td>
                            <td class="text-center has-edit-delete">
                                <div class="dropdown">
                                    <i class="fas fa-ellipsis-h" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <a class="dropdown-item" href="#"> Edit </a>
                                        <a class="dropdown-item text-danger" href="#">Remove</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Foundation Work</td>
                            <td>2008/11/13</td>
                            <td><span class="pending text-orange">Pending</span></td>
                            <td>Kampala</td>
                            <td>2011/01/25</td>
                            <td>25</td>
                            <td class="text-center has-edit-delete">
                                <div class="dropdown">
                                    <i class="fas fa-ellipsis-h" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>

                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                        <a class="dropdown-item" href="#"> Edit </a>
                                        <a class="dropdown-item text-danger" href="#">Remove</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>
@endsection
