(function ($) {
	"use strict";
		$(document).ready(function() {
			$('#reports-table').DataTable({
				searching: false,
				 paging: false,
				  info: false,
				  "columnDefs": [ {
					"targets": 'no-sort',
					"orderable": false,
			  } ]
			  
			});
		} );

		
		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});

		$(window).resize(function(e) {
			if($(window).width()<=768){
			$("#wrapper").removeClass("toggled");
			}else{
			$("#wrapper").addClass("toggled");
			}
		});

		$("[data-trigger]").on("click", function(e){
			e.preventDefault();
			e.stopPropagation();
			var offcanvas_id =  $(this).attr('data-trigger');
			$(offcanvas_id).toggleClass("show");
			$('body').toggleClass("offcanvas-active");
			$(".screen-overlay").toggleClass("show");
		}); 
		
		$(".btn-close, .screen-overlay").click(function(e){
		   $(".screen-overlay").removeClass("show");
		   $(".offcanvas").removeClass("show");
		   $("body").removeClass("offcanvas-active");
		});


    
	
		if (typeof document.getElementById("chart_widget_2") !== 'undefined' || document.getElementById("chart_widget_2") !== null) {
			let ctx = document.getElementById("chart_widget_2");
			ctx.height = 280;
			new Chart(ctx, {
				type: 'line',
				data: {
					labels: ["Oct 04", "Oct 05", "Oct 06", "Oct 07", "Oct 08", "Oct 09", "Oct 10"],
					type: 'line',
					defaultFontFamily: 'Lato',
					datasets: [{
						data:  [0, 300, 500, 600, 400, 600, 0],
						label: "complete",
						backgroundColor: '#324E8F',
						borderColor: '#324E8F',
						borderWidth: 0.5,
						pointStyle: 'circle',
						pointRadius: 5,
						pointBorderColor: 'transparent',
						pointBackgroundColor: '#324E8F',
					}, {
						label: "pending",
						data: [0, 1300, 400,1500, 100, 2000, 0, ],
						backgroundColor: '#FF8B42',
						borderColor: '#FF8B42',
						borderWidth: 0.5,
						pointStyle: 'circle',
						pointRadius: 5,
						pointBorderColor: 'transparent',
						pointBackgroundColor: '#FF8B42',
					}]
				},
				options: {
					responsive: true,
					maintainAspectRatio: false,
					tooltips: {
						mode: 'index',
						titleFontSize: 12,
						titleFontColor: '#fff',
						bodyFontColor: '#fff',
						backgroundColor: '#000',
						titleFontFamily: 'Lato',
						bodyFontFamily: 'Lato',
						cornerRadius: 3,
						intersect: false,
					},
					legend: {
						display: false,
						position: 'top',
						labels: {
							usePointStyle: true,
							fontFamily: 'Lato',
						},
		
		
					},
					scales: {
						xAxes: [{
							display: true,
							gridLines: {
								display: false,
								drawBorder: true
							},
							scaleLabel: {
								display: false,
								labelString: 'day'
							}
						}],
						yAxes: [{
							display: true,
							gridLines: {
								display: true,
								drawBorder: true
							},
							scaleLabel: {
								display: true,
								labelString: ' '
							},
							ticks: {
								beginAtZero: true,
	
								min: 0,
								max: 2500,
								stepSize: 500,
								callback: function (label, index, labels) {
									switch (label) {
										case 0:
											return '0';
										case 500:
											return '500';
										case 1000:
											return '1000';
										case 1500:
											return '1500';
										case 2000:
											return '2000';
										case 2500:
											return '2500';
	
									}
							}
						}
						}]
					},
					title: {
						display:false,
					}
				}
			});
		}
		
		
	

})(jQuery);