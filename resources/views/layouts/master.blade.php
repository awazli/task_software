<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.ico')}}">
    <!-- Place favicon.ico in the root directory -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Roboto:wght@300;400;500;700;900&display=swap"
          rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{asset('css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
</head>
<body>
<div class="app" id="app">
    <!-- Desktop Layout -->
    <div class="big-layout d-none d-lg-block">
        <header class="header position-absolute">
            <div class="header-block header-block-nav float-right">
                <ul class="nav-profile">
                    <li class="profile dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                            <div class="img d-inline-block" style="background-image: url({{asset('img/user.png')}})">
                            </div>
                            <span class="name d-inline-block"> Bruce Lee</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li align="">
                                <a href="#" class="btn btn-sm btn-default"><span
                                            class="glyphicon glyphicon-lock"></span> Account Settings</a>
                                <a href="{{route('logout')}}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" class="btn btn-sm btn-default"><span
                                            class="glyphicon glyphicon-log-out"></span> Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </header>

        <aside class="sidebar">
            <div class="sidebar-container">
                <div class="sidebar-header">
                    <div class="brand">
                        <div class="logo">
                            <img src="{{asset('img/left-logo.png')}}" class="w-100" alt="">
                        </div>
                    </div>
                </div>
                <nav class="menu">
                    <ul class="nav flex-column">
                        <li class="nav-item mb-21">
                            <a class="nav-link p-0 " href="#"><img src="{{asset('img/task.svg')}}"
                                                                           class="vertical-menu-icons" alt=""> Tasks</a>
                        </li>
                        <li class="nav-item mb-21">
                            <a class="nav-link p-0 active" href="#"><img src="{{asset('img/user.svg')}}"
                                                                                  class="vertical-menu-icons" alt="">
                                Users</a>
                        </li>
                        <li class="nav-item mb-21">
                            <a class="nav-link p-0" href="#"><img src="{{asset('img/reports.svg')}}"
                                                                             class="vertical-menu-icons" alt=""> Reports</a>
                        </li>
                        <li class="nav-item mb-21">
                            <a class="nav-link p-0" href="#"><img src="{{asset('img/privacy.svg')}}"
                                                                  class="vertical-menu-icons" alt=""> Privacy</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <footer class="sidebar-footer">
                <nav class="menu">
                    <ul class="sidebar-menu  nav" id="customize-menu">
                        <li class="nav-item">
                            <a class="nav-link p-0" href="{{route('logout')}}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><img src="{{asset('img/sign-out.svg')}}"
                                                                                   class="vertical-menu-icons" alt="">
                                Sign out</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </nav>
            </footer>
        </aside>
        @yield('content')
    </div>
    <!-- End Desktop Layout -->
    <!-- Mobile Layout  -->
    <div class="small-layout d-lg-none">
        <header class="header">
            <div class="container-fluid m-0 p-0">
                <div class="header-block header-block-nav d-flex justify-content-between align-items-center">
                    <nav class="navbar navbar-expand navbar-dark p-0">
                        <a href="#menu-toggle" id="menu-toggle" class=""><span
                                    class="navbar-toggler-icon"></span></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false"
                                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                    </nav>
                    <h1 class="mb-0 w-100 text-center dark-color font-weight-bold ln-50">Reports</h1>
                    <div class="icons d-flex">
                        <div class="notification-icon">
                            <button data-trigger="#my_offcanvas1" class="btn  bg-transparent border-0"
                                    type="button"></button>
                        </div>
                        <div class="mobile-task">
                            <button class="btn bg-transparent border-0" type="button"></button>
                        </div>
                    </div>
                </div>
                <!-- #wrapper -->
                <div id="wrapper">
                    <!-- Sidebar -->
                    <aside class="sidebar">
                        <div class="container-fluid">
                            <div class="sidebar-container">
                                <div class="sidebar-header">
                                    <div class="brand">
                                        <div class="logo">
                                            <img src="{{asset('img/left-logo.png')}}" class="w-100" alt="">
                                        </div>
                                    </div>
                                </div>
                                <nav class="menu">
                                    <ul class="nav flex-column">
                                        <li class="nav-item mb-21">
                                            <a class="nav-link p-0 active" href="#"><img
                                                        src="{{asset('img/task.svg')}}" class="vertical-menu-icons"
                                                        alt="">
                                                Tasks</a>
                                        </li>
                                        <li class="nav-item mb-21">
                                            <a class="nav-link p-0" href="#"><img
                                                        src="{{asset('img/user.svg')}}"
                                                        class="vertical-menu-icons"
                                                        alt=""> Users</a>
                                        </li>
                                        <li class="nav-item mb-21">
                                            <a class="nav-link p-0" href="#"><img src="{{asset('img/reports.svg')}}"
                                                                                  class="vertical-menu-icons" alt="">
                                                Reports</a>
                                        </li>
                                        <li class="nav-item mb-21">
                                            <a class="nav-link p-0" href="#"><img src="{{asset('img/privacy.svg')}}"
                                                                                  class="vertical-menu-icons" alt="">
                                                Privacy</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <footer class="sidebar-footer">
                                <nav class="menu">
                                    <ul class="sidebar-menu  nav">
                                        <li class="nav-item">
                                            <a class="nav-link p-0" href="{{route('logout')}}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><img src="{{asset('img/sign-out.svg')}}"
                                                                                   class="vertical-menu-icons" alt="">
                                                Sign out</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </nav>
                            </footer>
                        </div>
                    </aside>
                </div> <!-- /#wrapper -->
            </div>
        </header>
        <aside class="offcanvas notification-layout" id="my_offcanvas1">
            <header class="d-flex position-relative">
                <button class="btn btn-close"></button>
                <h1 class="mb-0 text-center w-100 dark-color font-weight-bold ln-50">Notifications </h1>
            </header>
            <ul class="list-group p-0" id="reports-list">
                <li class="list-group-item border-0 no-border-radius">
                    <div class="container-fluid m-0 p-0">
                        <div class="d-flex  p-0 align-items-center">
                            <div class="list-image">
                                <img src="{{asset('img/list-image.svg')}}" alt=""
                                     class="img-responsive rounded-circle user-pic"/>
                            </div>
                            <div class="list-title">
                                <h3 class="name">Foundation Work <span class="approval">recently approved by the</span>
                                    owner </h3>
                                <div class="date ln-24 font-weight-normal"><img src="{{asset('img/list-calender.svg')}}"
                                                                                class="mr-2" alt="">12-11-2020
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item border-0 no-border-radius">
                    <div class="container-fluid m-0 p-0">
                        <div class="d-flex  p-0 align-items-center">
                            <div class="list-image">
                                <img src="{{asset('img/list-image.svg')}}" alt=""
                                     class="img-responsive rounded-circle user-pic"/>
                            </div>
                            <div class="list-title">
                                <h3 class="name">Foundation Work <span class="approval">recently approved by the</span>
                                    owner </h3>
                                <div class="date ln-24 font-weight-normal"><img src="{{asset('img/list-calender.svg')}}"
                                                                                class="mr-2" alt="">12-11-2020
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item border-0 no-border-radius">
                    <div class="container-fluid m-0 p-0">
                        <div class="d-flex  p-0 align-items-center">
                            <div class="list-image">
                                <img src="{{asset('img/list-image.svg')}}" alt=""
                                     class="img-responsive rounded-circle user-pic"/>
                            </div>
                            <div class="list-title">
                                <h3 class="name">Foundation Work <span class="approval">recently approved by the</span>
                                    owner </h3>
                                <div class="date ln-24 font-weight-normal"><img src="{{asset('img/list-calender.svg')}}"
                                                                                class="mr-2" alt="">12-11-2020
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </li>
            </ul>
        </aside>
        <article class=" dashboard-page">
            <div class="search-bar">
                <div class="container-fluid p-0 m-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                </div>
            </div>
            <ul class="list-group p-0" id="reports-list">
                <span class="date">Today</span>
                <li class="list-group-item border-0 no-border-radius">
                    <div class="container-fluid m-0 p-0">
                        <div class="d-flex  p-0 align-items-center">
                            <div class="list-image">
                                <img src="{{asset('img/list-image.svg')}}" alt=""
                                     class="img-responsive rounded-circle user-pic"/>
                            </div>
                            <div class="list-title">
                                <h3 class="name"><span class="list-id">3660 :</span> Foundation Work</h3>
                                <div class="date ln-24 font-weight-normal"><img src="{{asset('img/list-calender.svg')}}"
                                                                                class="mr-2" alt="">12-11-2020
                                </div>
                            </div>
                            <div class="list-check ml-auto">
                                <img src="{{asset('img/tik-mark.svg')}}" class="t" alt="">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item border-0 no-border-radius">
                    <div class="container-fluid m-0 p-0">
                        <div class="d-flex  p-0 align-items-center">
                            <div class="list-image">
                                <img src="{{asset('img/list-image.svg')}}" alt=""
                                     class="img-responsive rounded-circle user-pic"/>
                            </div>
                            <div class="list-title">
                                <h3 class="name"><span class="list-id">3660 :</span> Foundation Work</h3>
                                <div class="date ln-24 font-weight-normal"><img src="{{asset('img/list-calender.svg')}}"
                                                                                class="mr-2" alt="">12-11-2020
                                </div>
                            </div>
                            <div class="list-check ml-auto">
                                <img src="{{asset('img/tik-mark.svg')}}" class="t" alt="">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item border-0 no-border-radius">
                    <div class="container-fluid m-0 p-0">
                        <div class="d-flex  p-0 align-items-center">
                            <div class="list-image">
                                <img src="{{asset('img/list-image.svg')}}" alt=""
                                     class="img-responsive rounded-circle user-pic"/>
                            </div>
                            <div class="list-title">
                                <h3 class="name"><span class="list-id">3660 :</span> Foundation Work</h3>
                                <div class="date ln-24 font-weight-normal"><img src="{{asset('img/list-calender.svg')}}"
                                                                                class="mr-2" alt="">12-11-2020
                                </div>
                            </div>
                            <div class="list-check ml-auto">
                                <img src="{{asset('img/tik-mark.svg')}}" class="t" alt="">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="list-group p-0" id="reports-list">
                <span class="date">13-11-2020</span>
                <li class="list-group-item border-0 no-border-radius">
                    <div class="container-fluid m-0 p-0">
                        <div class="d-flex  p-0 align-items-center">
                            <div class="list-image">
                                <img src="{{asset('img/list-image.svg')}}" alt=""
                                     class="img-responsive rounded-circle user-pic"/>
                            </div>
                            <div class="list-title">
                                <h3 class="name"><span class="list-id">3660 :</span> Foundation Work</h3>
                                <div class="date ln-24 font-weight-normal"><img src="{{asset('img/list-calender.svg')}}"
                                                                                class="mr-2" alt="">12-11-2020
                                </div>
                            </div>
                            <div class="list-check ml-auto">
                                <img src="{{asset('img/tik-mark.svg')}}" class="t" alt="">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item border-0 no-border-radius">
                    <div class="container-fluid m-0 p-0">
                        <div class="d-flex  p-0 align-items-center">
                            <div class="list-image">
                                <img src="{{asset('img/list-image.svg')}}" alt=""
                                     class="img-responsive rounded-circle user-pic"/>
                            </div>
                            <div class="list-title">
                                <h3 class="name"><span class="list-id">3660 :</span> Foundation Work</h3>
                                <div class="date ln-24 font-weight-normal"><img src="{{asset('img/list-calender.svg')}}"
                                                                                class="mr-2" alt="">12-11-2020
                                </div>
                            </div>
                            <div class="list-check ml-auto">
                                <img src="{{asset('img/tik-mark.svg')}}" class="t" alt="">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item border-0 no-border-radius">
                    <div class="container-fluid m-0 p-0">
                        <div class="d-flex  p-0 align-items-center">
                            <div class="list-image">
                                <img src="{{asset('img/list-image.svg')}}" alt=""
                                     class="img-responsive rounded-circle user-pic"/>
                            </div>
                            <div class="list-title">
                                <h3 class="name"><span class="list-id">3660 :</span> Foundation Work</h3>
                                <div class="date ln-24 font-weight-normal"><img src="{{asset('img/list-calender.svg')}}"
                                                                                class="mr-2" alt="">12-11-2020
                                </div>
                            </div>
                            <div class="list-check ml-auto">
                                <img src="{{asset('img/tik-mark.svg')}}" class="t" alt="">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </li>
            </ul>
        </article>
    </div>
    <!-- End Mobile Layout  -->
</div>
</body>
<!-- JS here -->
<script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
<script src="{{asset('js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</html>