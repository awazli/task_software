@extends('layouts.app')

@section('content')
    <div class="offset-1 col-lg-5 login-sec  justify-content-center align-self-center text-center">
        <div class="mobile-view  d-lg-flex d-flex justify-content-center align-items-center">
            <div class="login-text d-lg-flex d-md-flex d-block mx-auto text-center mb-66 ">
                <img src="{{asset('img/Icon.svg')}}" alt="" class="login-logo mr-lg-3 mr-md-3 ">
                <h2 class="text-left h2 ln-45">Log In</h2>
            </div>
        </div>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group text-left">
                <label for="email" class="">Email</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                       name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group text-left">
                <label for="password" class="InputPassword1">Password</label>
                <input id="password" type="password"
                       class="form-control @error('password') is-invalid @enderror InputPassword1"
                       name="password" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-check text-left">
                <label for="remember" class="form-check-label blue font-weight-normal ln-26 font-15">
                    <input class="form-check-input" type="checkbox" name="remember"
                           id="remember" {{ old('remember') ? 'checked' : '' }}>
                    Remember me next time
                </label>

                <button type="submit" class="btn btn-login float-right btn-primary">Log In</button>
            </div>
        </form>
        <hr>
        @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}"
               class="text-center d-none d-lg-block d-sm-block mx-auto w-100 blue ln-17 font-14">Forgot
                Password?</a>
        @endif
    </div>
@endsection
