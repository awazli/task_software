@extends('layouts.app')

@section('content')
    <div class=" offset-1 col-lg-5 login-sec  justify-content-center align-self-center text-center">
        <div class="mobile-view  d-lg-flex d-flex justify-content-center align-items-center">
            <div class="login-text d-lg-flex d-md-flex d-block mx-auto text-center mb-66 ">
                <img src="{{asset('img/Icon.svg')}}" alt="" class="login-logo mr-lg-3 mr-md-3 ">
                <h2 class="text-left h2 ln-45">Reset Password</h2>
            </div>
        </div>
        <form method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group text-left">
                <label for="email" class="col-form-label">Email</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                       value="{{ $email ?? old('email') }}" required autocomplete="email"
                       placeholder="gigliopierluigi@gmail.com" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group text-left">
                <label for="password" class="col-form-label">Password</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                       name="password" required autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group text-left">
                <label for="password-confirm" class="col-form-label">Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required
                       autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class=" form-group d-lg-flex d-md-flex d-block justify-content-between align-items-center">
                <div class=" text-center pl-0 pb-lg-0 pb-md-0 pb-4">
                    Have an Account?
                    <a href="{{route('login')}}" class="text-center mx-auto w-100 blue ln-17 font-14">Sign in </a>
                </div>
                <button type="submit" class="btn btn-login float-right btn-primary">Submit</button>
            </div>
        </form>
        <hr>
    </div>
@endsection